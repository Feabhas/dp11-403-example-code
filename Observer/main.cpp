#include "Sensor.h"
#include "SerialPort.h"
#include "UI.h"

#include <chrono>
#include <thread>

using namespace std;
using namespace chrono;
using namespace chrono_literals;

int main() {
  Sensor sensor{};
  UI ui{sensor};
  SerialPort sp{sensor};

  sensor.attach(ui); // Subscribe to updates.
  sensor.attach(sp); // Subscribe to updates.

  for (int i{}; i < 10; ++i) {
    sensor.read();
    std::this_thread::sleep_for(1s);
  }
}
