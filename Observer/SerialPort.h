#ifndef SerialPort_H
#define SerialPort_H

#include "I_Observer.h"

class Sensor;

class SerialPort : public I_Observer {
public:
  SerialPort(Sensor &s) : sensor(&s) {}

protected:
  void update() override;

private:
  Sensor *sensor{};
};

#endif // SerialPort_H