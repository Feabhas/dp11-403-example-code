#include "SerialPort.h"
#include "Sensor.h"

#include <iostream>
using namespace std;

void SerialPort::update() {
  cout << "SerialPort::Update : ";
  cout << sensor->value() << endl;
}
