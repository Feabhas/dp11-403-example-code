#include "UI.h"
#include "Sensor.h"

#include <iostream>
using namespace std;

void UI::update() {
  // The UI probably doesn't want to update
  // every change in the sensor reading; so
  // we'll update every ten samples
  //
  ++count;
  //  if(count == 10)
  {
    cout << "UI::Update : ";
    cout << sensor->value() << endl;
    count = 0;
  }
}
