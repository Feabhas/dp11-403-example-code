#include "Sensor.h"
#include <cstdlib>
#include <ctime>

Sensor::Sensor() {
  std::srand(std::time(0)); // use current time as seed for random generator
}

void Sensor::read() {
  // Read the sensor and update
  // the internal state of the
  // Sensor
  sensor_reading = std::rand() / (double)RAND_MAX * 100.0;

  Subject::notify();
}

double Sensor::value() { return sensor_reading; }
