#ifndef UI_H
#define UI_H

#include "I_Observer.h"

class Sensor;

class UI : public I_Observer {
public:
  UI(Sensor &s) : sensor(&s) {}

protected:
  void update() override;

private:
  unsigned count{0};
  Sensor *sensor{};
};

#endif // UI_H