#ifndef Sensor_H
#define Sensor_H

#include "Subject.h"

class Sensor : public Subject {
public:
  Sensor();
  void read();
  double value();

private:
  double sensor_reading{};
};

#endif // Sensor_H