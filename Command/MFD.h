#ifndef MFD_H
#define MFD_H

#include "SoftKey.h"
#include <array>

class I_Command;

class MFD {
public:
  enum class KeyID { sk1, sk2, sk3, sk4 };
  void set_behaviour(KeyID key, I_Command &cmd);

  void SK_pressed(KeyID key);

private:
  static constexpr inline std::size_t num_keys{4};
  std::array<SoftKey, num_keys> keys;
};

#endif // MFD_H