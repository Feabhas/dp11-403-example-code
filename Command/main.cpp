#include "Commands.h"
#include "MFD.h"

#include <iostream>
using namespace std;

int main() {
  MFD display{};

  JettisonStore jettison{};
  //  GroundMovingTarget gnd_move_tgt  { };
  IncreaseChaff incr_chaff{};
  DecreaseChaff decr_chaff{};
  ClearTarget clear_tgt{};
  //  SteerToTarget      steer_tgt  { };
  NullCmd null{};

  display.set_behaviour(MFD::KeyID::sk1, jettison);
  display.set_behaviour(MFD::KeyID::sk2, null);

  display.SK_pressed(MFD::KeyID::sk1);
  display.SK_pressed(MFD::KeyID::sk2);

  cout << "+++ Changing Mode +++" << endl;
  display.set_behaviour(MFD::KeyID::sk1, incr_chaff);
  display.set_behaviour(MFD::KeyID::sk2, decr_chaff);

  display.SK_pressed(MFD::KeyID::sk1);
  display.SK_pressed(MFD::KeyID::sk2);
}
