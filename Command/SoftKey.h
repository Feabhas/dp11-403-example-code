#ifndef SoftKey_H
#define SoftKey_H

class I_Command;

class SoftKey {
public:
  void on_select();

private:
  friend void connect(SoftKey &key, I_Command &cmd);
  I_Command *command{};
};

#endif // SoftKey_H