#include "MFD.h"
#include "I_Command.h"

void MFD::set_behaviour(KeyID key, I_Command &cmd) {
  connect(keys.at(static_cast<std::size_t>(key)), cmd);
}

void MFD::SK_pressed(KeyID key) {
  keys.at(static_cast<std::size_t>(key)).on_select();
}
