#include "Commands.h"

#include <iostream>
using namespace std;

void ClearTarget::execute() {
  cout << "ClearTarget::execute()" << endl;
  // radar->mf()
}

void JettisonStore::execute() {
  cout << "JettisonStore::execute()" << endl;
  // weapon_rack->mf()
}

void DecreaseChaff::execute() { cout << "DecreaseChaff::execute()" << endl; }

void IncreaseChaff::execute() { cout << "IncreaseChaff::execute()" << endl; }
