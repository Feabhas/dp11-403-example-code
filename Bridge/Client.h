#ifndef Client_H
#define Client_H

#include <list>
class I_Device;

class Client {
public:
  void all_on();
  void all_off();

private:
  std::list<I_Device *> devices{};
  friend void connect(Client &c, I_Device &dev);
};

#endif // Client_H