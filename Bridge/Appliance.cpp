#include "Appliance.h"

#include <iostream>
using namespace std;

void Appliance::on() {
  cout << "Appliance on." << endl;
  Module::send_message(Module::Message::on);
}

void Appliance::off() {
  cout << "Appliance off." << endl;
  Module::send_message(Module::Message::off);
}
