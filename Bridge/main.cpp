#include "Appliance.h"
#include "Bluetooth.h"
#include "Client.h"
#include "Lamp.h"
#include "X10.h"

int main() {
  Bluetooth bluetooth{};
  X10 x10{};

  Lamp desk{Module::Location::room_A, Module::Device::dev_1};
  connect(desk, x10);

  Lamp study{Module::Location::room_B, Module::Device::dev_1};
  connect(study, bluetooth);

  Appliance porch{Module::Location::room_B, Module::Device::dev_2};
  connect(porch, bluetooth);

  Client client{};
  connect(client, desk);
  connect(client, study);
  connect(client, porch);

  client.all_on(); // Comms via X10

  client.all_off(); // Comms via Bluetooth
}
