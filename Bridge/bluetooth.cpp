#include "Bluetooth.h"

#include <iostream>
using namespace std;

void Bluetooth::send(const char *msg) {
  cout << "Bluetooth sending message... " << msg << endl;
}

bool Bluetooth::get_ack() { return true; }
