#include "Client.h"
#include "I_Device.h"

void Client::all_on() {
  for (auto &device : devices) {
    device->on();
  }
}

void Client::all_off() {
  for (auto &device : devices) {
    device->off();
  }
}

void connect(Client &c, I_Device &dev) { c.devices.push_back(&dev); }
