#ifndef Bluetooth_H
#define Bluetooth_H

#include "I_Comms.h"

class Bluetooth : public I_Comms {
protected:
  void send(const char *msg) override;
  bool get_ack() override;
};

#endif // Bluetooth_H