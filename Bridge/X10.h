#ifndef X10_H
#define X10_H

#include "I_Comms.h"

class X10 : public I_Comms {
protected:
  void send(const char *msg) override;
  bool get_ack() override;
};

#endif // X10_H