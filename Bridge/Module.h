#ifndef Module_H
#define Module_H

#include "I_Device.h"
#include <string>
class I_Comms;

class Module : public I_Device {
public:
  enum class Location { unknown, room_A, room_B };
  enum class Device { invalid, dev_1, dev_2, dev_3, dev_4 };

  Module(Location loc, Device dev) : location{loc}, device{dev} {}

protected:
  virtual void on() = 0;
  virtual void off() = 0;
  std::string display_name();

protected:
  enum class Message { on, off };
  std::string translate_to_chars(Message msg);
  void send_message(Message msg);

private:
  I_Comms *comms{};
  friend void connect(Module &dev, I_Comms &coms);

  Location location{};
  Device device{};
};

#endif // Module_H