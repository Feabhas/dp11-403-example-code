#include "Module.h"
#include "I_Comms.h"
#include <array>
#include <string>
using namespace std;

constexpr array<string_view, 3> Location_names{"UNKNOWN", "ROOM A", "ROOM B"};
constexpr array<string_view, 5> Device_names{"INVALID", "DEV 1", "DEV 2","DEV 3", "DEV 4"};

std::string Module::display_name() {
  const string disp_name{
      string{Location_names.at(static_cast<size_t>(location))} + ":" +
      string{Device_names.at(static_cast<size_t>(device))}};
  return disp_name;
}

std::string Module::translate_to_chars(Message msg) {
  if (msg == Message::on)
    return display_name() + " On";
  return display_name() + " Off";
}

void Module::send_message(Message msg) {
  const std::string message = translate_to_chars(msg);

  if (comms) {
    comms->send(message.c_str());

    if (!comms->get_ack()) {
      // Error handling...
    }
  }
}

void connect(Module &dev, I_Comms &coms) { dev.comms = &coms; }
