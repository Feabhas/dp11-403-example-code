#ifndef Lamp_H
#define Lamp_H

#include "Module.h"

class Lamp : public Module {
public:
  Lamp(Module::Location loc, Module::Device dev) : Module{loc, dev} {}

protected:
  void on() override;
  void off() override;
};

#endif // Lamp_H