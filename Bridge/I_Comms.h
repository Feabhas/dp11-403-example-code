#ifndef I_Comms_H
#define I_Comms_H

// 'Body' interface
//
class I_Comms {
public:
  virtual void send(const char *msg) = 0;
  virtual bool get_ack() = 0;
  virtual ~I_Comms() = default;
};

#endif // I_Comms_H