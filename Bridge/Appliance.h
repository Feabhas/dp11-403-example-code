#ifndef Appliance_H
#define Appliance_H

#include "Module.h"

class Appliance : public Module {
public:
  Appliance(Module::Location loc, Module::Device dev) : Module{loc, dev} {}

protected:
  void on() override;
  void off() override;
};

#endif // Appliance_H
