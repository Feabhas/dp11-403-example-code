#include "X10.h"

#include <iostream>
using namespace std;

void X10::send(const char *msg) {
  cout << "X10 sending message... " << msg << endl;
}

bool X10::get_ack() { return true; }
