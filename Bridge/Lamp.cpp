#include "Lamp.h"

#include <iostream>
using namespace std;

void Lamp::on() {
  cout << "Lamp on." << endl;
  Module::send_message(Module::Message::on);
}

void Lamp::off() {
  cout << "Lamp off." << endl;
  Module::send_message(Module::Message::off);
}
