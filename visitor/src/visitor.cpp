#include <initializer_list>
#include <iostream>
#include <list>
#include <string_view>
#include <vector>

class IVisitor;

class Document_element {
public:
  explicit Document_element(std::string_view sv) : text{sv} {}
  virtual void accept(IVisitor &visitor) = 0;
  std::string part() { return text; }

protected:
  std::string text{};
};

class Plain_text_elem;
class Bold_text_elem;
class Hyperlink_elem;

class IVisitor {
public:
  virtual void visit(Plain_text_elem &docPart) = 0;
  virtual void visit(Bold_text_elem &docPart) = 0;
  virtual void visit(Hyperlink_elem &docPart) = 0;
};

class Plain_text_elem : public Document_element {
public:
  explicit Plain_text_elem(std::string_view sv) : Document_element{sv} {}
  void accept(IVisitor &visitor) override { visitor.visit(*this); }
};

class Bold_text_elem : public Document_element {
public:
  explicit Bold_text_elem(std::string_view sv) : Document_element{sv} {}
  void accept(IVisitor &visitor) override { visitor.visit(*this); }
};

class Hyperlink_elem : public Document_element {
public:
  Hyperlink_elem(std::string_view sv, std::string_view url)
      : Document_element{sv}, Url{url} {}

  std::string url() { return Url; }
  void accept(IVisitor &visitor) override { visitor.visit(*this); }

private:
  std::string Url{};
};

class HtmlVisitor : public IVisitor {
public:
  void visit(Plain_text_elem &docPart) override;
  void visit(Bold_text_elem &docPart) override;
  void visit(Hyperlink_elem &docPart) override;

  operator char const *() const { return m_output.c_str(); }

protected:
  std::string m_output{};
};

void HtmlVisitor::visit(Plain_text_elem &docPart) {
  m_output += docPart.part() + '\n';
}

void HtmlVisitor::visit(Bold_text_elem &docPart) {
  m_output += "<b>" + docPart.part() + "</b>\n";
}

void HtmlVisitor::visit(Hyperlink_elem &docPart) {
  m_output +=
      R"(<a href=")" + docPart.url() + R"(">)" + docPart.part() + "</a>\n";
}

class MarkdownVisitor : public IVisitor {
public:
  void visit(Plain_text_elem &docPart) override;
  void visit(Bold_text_elem &docPart) override;
  void visit(Hyperlink_elem &docPart) override;
  operator char const *() const { return m_output.c_str(); }

private:
  std::string m_output{};
};

void MarkdownVisitor::visit(Plain_text_elem &docPart) {
  m_output += docPart.part() + '\n';
}

void MarkdownVisitor::visit(Bold_text_elem &docPart) {
  m_output += "**" + docPart.part() + "**\n";
}

void MarkdownVisitor::visit(Hyperlink_elem &docPart) {
  m_output += "[" + docPart.part() + "](" + docPart.url() + ")\n";
}

class Document {
public:
  Document(std::initializer_list<Document_element *> docs) : m_parts{docs} {}
  void accept(IVisitor &visitor) {
    for (auto &part : m_parts) {
      part->accept(visitor);
    }
  }

private:
  std::vector<Document_element *> m_parts{};
};

int main() {
  Plain_text_elem pt{"simple plain text"};
  Bold_text_elem bt{"bold text"};
  Hyperlink_elem ht{"Compiler explorer", "https://goldbolt.org"};

  Document doc{&pt, &bt, &ht};

  MarkdownVisitor md_visitor{};
  doc.accept(md_visitor);
  std::cout << md_visitor << '\n';

  HtmlVisitor htmlVisitor{};
  doc.accept(htmlVisitor);
  std::cout << htmlVisitor << '\n';
}