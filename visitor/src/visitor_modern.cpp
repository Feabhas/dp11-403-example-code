#include <initializer_list>
#include <iostream>
#include <list>
#include <variant>
#include <vector>

template <typename... Lambda_Ty> class Visitor : public Lambda_Ty... {
public:
  using Lambda_Ty::operator()...;
};

template <typename... T> Visitor(T...) -> Visitor<T...>;

class IVisitor;

class DocumentPart {
public:
  explicit DocumentPart(std::string_view sv) : text{sv} {}
  virtual void accept(IVisitor &visitor) = 0;
  std::string part() { return text; }

protected:
  std::string text{};
};

class PlainText;
class BoldText;
class Hyperlink;

class IVisitor {
public:
  virtual void visit(PlainText &docPart) = 0;
  virtual void visit(BoldText &docPart) = 0;
  virtual void visit(Hyperlink &docPart) = 0;
};

class PlainText : public DocumentPart {
public:
  explicit PlainText(std::string_view sv) : DocumentPart{sv} {}
  void accept(IVisitor &visitor) override { visitor.visit(*this); }
};

class BoldText : public DocumentPart {
public:
  explicit BoldText(std::string_view sv) : DocumentPart{sv} {}
  void accept(IVisitor &visitor) override { visitor.visit(*this); }
};

class Hyperlink : public DocumentPart {
public:
  Hyperlink(std::string_view sv, std::string_view url)
      : DocumentPart{sv}, Url{url} {}

  std::string url() { return Url; }
  void accept(IVisitor &visitor) override { visitor.visit(*this); }

private:
  std::string Url{};
};

using doc_text = std::variant<PlainText, BoldText, Hyperlink>;

int main() {
  PlainText pt{"simple plain text"};
  BoldText bt{"bold text"};
  Hyperlink ht{"Compiler explorer", "https://goldbolt.org"};

  std::vector<doc_text> Document{pt, bt, ht};

  //   std::string txt{};
  //   Visitor md_visitor{
  //       [&txt](PlainText &docPart) { txt += docPart.part() + '\n'; },
  //       [&txt](BoldText &docPart) { txt += "**" + docPart.part() + "**\n"; },
  //       [&txt](Hyperlink &docPart) {
  //         txt += "[" + docPart.part() + "](" + docPart.url() + ")\n";
  //       },
  //   };

  //   for (auto& part : Document) {
  //     std::visit(md_visitor, part);
  //   }

  //   std::cout << txt << '\n';

  std::string txt{};
  for (auto &part : Document) {
    std::visit(Visitor{
                   [&txt](PlainText &docPart) { txt += docPart.part() + '\n'; },
                   [&txt](BoldText &docPart) {
                     txt += "**" + docPart.part() + "**\n";
                   },
                   [&txt](Hyperlink &docPart) {
                     txt += "[" + docPart.part() + "](" + docPart.url() + ")\n";
                   },
               },
               part);
  }
  std::cout << txt << '\n';
}
