#include "UI.h"

#include "Controller.h"

#include <chrono> // namespace chrono
#include <iostream>
#include <thread> // namespace this_thread

using namespace std;
using namespace chrono;
using namespace chrono_literals;

bool UI::run() {
  // Whatever processing needs doing...
  //
  this_thread::sleep_for(100ms);

  cout << "Message from UI" << endl;
  controller->async_op();

  return false;
}
