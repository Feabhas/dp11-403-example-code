#include <iostream>
#include <thread>
using namespace std;

#include "CommsTask.h"
#include "Controller.h"
#include "UI.h"

int main() {
  Controller c{};
  UI ui{c};
  CommsTask ct{c};

  auto run_forever_policy = [](I_Runnable &runnable) {
    while (true)
      runnable.run();
  };
  auto run_num_policy = [](I_Runnable &runnable, int count) {
    for (int i{}; i < count; ++i)
      runnable.run();
  };

  thread t1{run_forever_policy, std::ref(c)};
  thread t2{run_num_policy, std::ref(ui), 10};
  thread t3{run_num_policy, std::ref(ct), 20};

  t1.detach();

  t2.join();
  t3.join();
}
