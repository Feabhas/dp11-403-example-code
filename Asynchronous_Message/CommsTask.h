#ifndef CommsTask_H
#define CommsTask_H

class Controller;
#include "I_Runnable.h"

class CommsTask : public I_Runnable {
public:
  // Client API
  CommsTask(Controller &c) : controller(&c) {}

protected:
  virtual bool run();

private:
  Controller *controller;
};

#endif // CommsTask_H
