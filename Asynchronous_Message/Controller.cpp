#include "Controller.h"

#include <iostream>
using namespace std;

void Controller::async_op() { msgQ.push(&Controller::async_impl); }

bool Controller::run() {
  auto op = msgQ.get();
  op(this);
  return true;
}

void Controller::async_impl() {
  // Implementation...
  cout << "Controller::async_impl" << endl;
}
