#ifndef Controller_H
#define Controller_H

#include "I_Runnable.h"
#include "MessageQueue.h"

#include <functional>

class Controller : public I_Runnable {
public:
  // Synchronous interface not shown...
  //

  // Asynchronous interface
  //
  void async_op();

protected:
  virtual bool run();

private:
  using mem_fn_ptr = std::function<void(Controller *)>;
  MessageQueue<mem_fn_ptr> msgQ;

  void async_impl();
};

#endif // Controller_H