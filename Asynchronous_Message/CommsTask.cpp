#include "CommsTask.h"

#include "Controller.h"

#include <chrono> // namespace chrono
#include <iostream>
#include <thread> // namespace this_thread

using namespace std;
using namespace chrono;
using namespace chrono_literals;

bool CommsTask::run() {
  // Whatever processing needs doing...
  //
  this_thread::sleep_for(110ms);

  cout << "Message from CommsTask" << endl;

  controller->async_op();

  return false;
}
