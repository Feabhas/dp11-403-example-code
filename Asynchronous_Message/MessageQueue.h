#ifndef MessageQueue_H
#define MessageQueue_H

#include <condition_variable>
#include <mutex>
#include <queue>

template <typename Ty> class MessageQueue {
public:
  bool push(Ty val);
  Ty get();

private:
  std::queue<Ty> q;

  std::mutex mtx;
  std::condition_variable has_data;
};

template <typename Ty> bool MessageQueue<Ty>::push(Ty val) {
  std::unique_lock<std::mutex> guard(mtx);
  q.push(val);
  has_data.notify_all();
  return true;
}

template <typename Ty> Ty MessageQueue<Ty>::get() {
  std::unique_lock<std::mutex> guard(mtx);
  has_data.wait(guard, [this] { return !q.empty(); });

  Ty val = q.front();
  q.pop();

  return val;
}

#endif // MessageQueue_H