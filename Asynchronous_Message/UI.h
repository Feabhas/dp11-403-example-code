#ifndef UI_H
#define UI_H

class Controller;
#include "I_Runnable.h"

class UI : public I_Runnable {
public:
  // Client API
  UI(Controller &c) : controller(&c) {}

protected:
  virtual bool run();

private:
  Controller *controller;
};

#endif // UI_H