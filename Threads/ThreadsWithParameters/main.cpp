#include <iostream>
#include <thread>

#include <iostream>
using namespace std;

class Functor
{
public:
  void operator()( char c, int count )
  {
    for ( int i = 0; i < count; ++i )
    {
      std::cout << c;
    }
  }
};

int main()
{
  std::thread t1{ Functor{}, '1', 60 };
  std::thread t2{ Functor{}, '2', 80 };

  t1.join();
  t2.join();

  cout << endl;
}
