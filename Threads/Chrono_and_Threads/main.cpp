#include <chrono> // namespace chrono
#include <iostream>
#include <thread> // namespace this_thread

using namespace std;
using namespace chrono;
using namespace chrono_literals;

void myThreadFunction()
{
  for ( int i = 0; i < 10; ++i )
  {
    cout << 'x';
    cout.flush();
    this_thread::sleep_for( 500ms );
  }
}

int main()
{
  std::thread t1{ myThreadFunction };
  t1.join();
  cout << endl;
}
