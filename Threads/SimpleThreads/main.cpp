#include <iostream>
#include <thread>

#include <iostream>
using namespace std;

class Functor
{
public:
  void operator()()
  {
    for ( int i = 0; i < 100; ++i )
    {
      std::cout << 'x';
    }
  }
};

void free_function()
{
  for ( int i = 0; i < 100; ++i )
  {
    std::cout << 'y';
  }
}

int main()
{
  std::thread t1{ free_function };
  std::thread t2{ Functor{} };
  std::thread t3{ []() {
    for ( int i = 0; i < 100; ++i )
      cout << 'z';
  } };

  t1.join();
  t2.join();
  t3.join();

  cout << endl;
}
