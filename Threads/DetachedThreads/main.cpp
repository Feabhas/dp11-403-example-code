#include <iostream>
#include <thread>

#include <iostream>
using namespace std;

void myThreadFunction()
{
  for ( int i = 0; i < 100; ++i )
  {
    std::cout << i;
  }
}

int main()
{
  std::thread t1{ myThreadFunction }; // t1 is joinable

  t1.detach(); // t1 is non-joinable

  for ( int i = 0; i < 10; ++i )
  {
    std::cout << 'm';
  }

  if ( t1.joinable() ) {
    t1.join(); // t1 is non-joinable
  }

  cout << endl;
}
