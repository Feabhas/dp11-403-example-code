#if 1

#include <chrono>
#include <cstring>
#include <iostream>
#include <pthread.h>
#include <thread>

auto lambda = []( char c ) {
  std::this_thread::sleep_for( std::chrono::seconds( 1 ) );
  for ( int i{}; i < 100; ++i )
    std::cout << c;
};

int main()
{
  std::thread t1{ lambda, '1' };
  std::thread t2{ lambda, '2' };

  auto handle = t1.native_handle();

#if __GNUC__
  std::cout << "Setting priority" << std::endl;
  sched_param sch;
  int         policy;
  pthread_getschedparam( handle, &policy, &sch );
  sch.sched_priority = 20;
  auto err           = pthread_setschedparam( handle, SCHED_FIFO, &sch );
  if ( err ) {
    std::cout << "Failed to setschedparam: " << std::strerror( errno )
              << std::endl;
  }
#endif

  t1.join();
  t2.join();
}

#else
#include <chrono>
#include <cstring>
#include <iostream>
#include <mutex>
#include <pthread.h>
#include <thread>

std::mutex iomutex;
void       f( int num )
{
  std::this_thread::sleep_for( std::chrono::seconds( 1 ) );

  sched_param sch;
  int         policy;
  pthread_getschedparam( pthread_self(), &policy, &sch );
  std::lock_guard<std::mutex> lk( iomutex );
  std::cout << "Thread " << num << " is executing at priority "
            << sch.sched_priority << '\n';
}

int main()
{
  std::thread t1{ f, 1 };
  std::thread t2{ f, 2 };

  sched_param sch;
  int         policy;
  auto        handle = t1.native_handle();
  pthread_getschedparam( handle, &policy, &sch );
  auto err = pthread_setschedparam( t1.native_handle(), SCHED_FIFO, &sch );
  sch.sched_priority = 20;
  if ( err ) {
    std::cout << "Failed to setschedparam: " << std::strerror( errno ) << '\n';
  }

  t1.join();
  t2.join();
}

#endif