#include <iostream>
#include <thread>

#include <iostream>
using namespace std;

class I_Runnable
{
public:
  virtual void run()    = 0;
  virtual ~I_Runnable() = default;
};

class RunnableObject : public I_Runnable
{
public:
  RunnableObject( char c ) : ch{ c } {}
  void run() override;

private:
  char ch{ '#' };
};

void RunnableObject::run()
{
  std::cout << ch;
}

int main()
{
  RunnableObject obj1{ 'o' };
  RunnableObject obj2{ 'O' };

  auto run_policy = []( I_Runnable& runnable ) {
    for ( int i{}; i < 100; ++i )
      runnable.run();
  };

  thread t1{ run_policy, std::ref( obj1 ) };
  thread t2{ run_policy, std::ref( obj2 ) };

  t1.join();
  t2.join();

  cout << endl;
}
