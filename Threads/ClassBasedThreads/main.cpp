#include <iostream>
#include <thread>

#include <iostream>
using namespace std;

class ThreadedObject
{
public:
  ThreadedObject( char c ) : ch{ c } {}
  void run() const;

private:
  char ch;
};

void ThreadedObject::run() const
{
  for ( int i = 0; i < 100; ++i )
  {
    std::cout << ch;
  }
}

int main()
{
  ThreadedObject obj{ '.' };

  std::thread t1{ &ThreadedObject::run, &obj };

  t1.join();

  cout << endl;
}
