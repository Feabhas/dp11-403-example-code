#ifndef Consumer_H
#define Consumer_H

#include "I_Runnable.h"

class SimpleStack;

class Consumer : public I_Runnable {
public:
  Consumer(SimpleStack &s, char c);

  void run();
  int get_sum() const { return sum; }

private:
  int sum{};
  SimpleStack *stack{};
  char ch{};
};

#endif // Consumer_H