#include "Consumer.h"

#include "SimpleStack.h"

#include <chrono> // namespace chrono
#include <iostream>
#include <thread> // namespace this_thread

using namespace std;
using namespace chrono;
using namespace chrono_literals;

Consumer::Consumer(SimpleStack &s, char c) : stack{&s}, ch{c} {}

void Consumer::run() {
  do {
    this_thread::sleep_for(100ms);
    cout << ch;
    cout.flush();
    sum += stack->pop();
  } while (true);
}
