#include "SimpleStack.h"

#include <chrono>
#include <thread>
using namespace std;

#if 0
bool SimpleStack::push(int val)
{
  unique_lock<mutex> guard(mtx);
  while(count == sz)
  {
    has_space.wait(guard);
  }
  stack[count++] = val;

  has_data.notify_all();
  return true;
}

int SimpleStack::pop()
{
  unique_lock<mutex> guard(mtx);
  while(count == 0)
  {
    has_data.wait(guard);
  }
  int val = stack[--count];

  has_space.notify_all();
  return val;
}

#else
bool SimpleStack::push(int val) {
  { // Critical section

    unique_lock<mutex> guard(mtx);
    has_space.wait(guard, [this] { return count != sz; });

    stack[count++] = val;

  } // End critical section

  has_data.notify_all();
  return true;
}

int SimpleStack::pop() {
  int val;
  {
    unique_lock<mutex> guard(mtx);
    has_data.wait(guard, [this] { return count != 0; });

    val = stack[--count];
  }
  has_space.notify_all();
  return val;
}

#endif
