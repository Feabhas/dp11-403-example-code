#include <iostream>
#include <thread>
using namespace std;

#include "Consumer.h"
#include "Producer.h"
#include "SimpleStack.h"

int main() {
  SimpleStack ss{};
  Producer p{ss};
  Consumer c1{ss, '.'};
  Consumer c2{ss, '~'};

  auto run_policy = [](I_Runnable &runnable) { runnable.run(); };

  thread t1{run_policy, std::ref(p)};
  thread t2{run_policy, std::ref(c1)};
  thread t3{run_policy, std::ref(c2)};

  t2.detach();
  t3.detach();

  t1.join();

  this_thread::sleep_for(2s);

  cout << "c1 Consumed : " << c1.get_sum() << endl;
  cout << "c2 Consumed : " << c2.get_sum() << endl;
}
