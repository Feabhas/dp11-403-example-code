#ifndef Producer_H
#define Producer_H

#include "I_Runnable.h"

class SimpleStack;

class Producer : public I_Runnable {
public:
  Producer(SimpleStack &s) : stack{&s} {}

  void run();

private:
  int sum{};
  SimpleStack *stack{};
};

#endif // Producer_H