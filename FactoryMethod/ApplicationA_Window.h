#ifndef ApplicationA_Window_H
#define ApplicationA_Window_H

#include "App_Window.h"
// Application A
//
class ApplicationA_Window : public Window {
public:
  std::unique_ptr<I_Menu> make_menu() override;
};

#endif
