#include "App_Window.h"

void Window::show_menu() {
  auto menu = make_menu();
  menu->select();
}
