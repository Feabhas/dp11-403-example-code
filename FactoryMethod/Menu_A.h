#ifndef Menu_A_H
#define Menu_A_H

#include "I_Menu.h"

// Application A
//
class Menu_A : public I_Menu {
public:
  void select() override;
  ~Menu_A() override = default;
};

#endif
