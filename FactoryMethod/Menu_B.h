#ifndef Menu_B_H
#define Menu_B_H

#include "I_Menu.h"
// Application A
//
class Menu_B : public I_Menu {
public:
  void select() override;
  ~Menu_B() override = default;
};

#endif
