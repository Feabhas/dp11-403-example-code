#include "ApplicationA_Window.h"
#include "Menu_A.h"

std::unique_ptr<I_Menu> ApplicationA_Window::make_menu() {
  return std::make_unique<Menu_A>();
}
