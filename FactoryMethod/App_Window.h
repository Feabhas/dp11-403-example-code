#ifndef App_Window_H
#define App_Window_H

#include "I_Menu.h"
#include <memory>
// Framework code
//
class Window {
public:
  void show_menu();
  virtual std::unique_ptr<I_Menu> make_menu() = 0;
};

#endif
