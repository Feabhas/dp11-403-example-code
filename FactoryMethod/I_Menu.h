#ifndef I_MENU_H
#define I_MENU_H

class I_Menu {
public:
  virtual void select() = 0;
  virtual ~I_Menu() = default;
};

#endif
