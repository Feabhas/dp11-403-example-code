#include "HTMLDocBuilder.h"

void HTMLDocBuilder::header(std::string_view str) {
  doc << "<header>" << str << "</header>\n";
}

void HTMLDocBuilder::body(std::string_view str) {
  doc << "<body>" << str << "</body>\n";
}

void HTMLDocBuilder::footer(std::string_view str) {
  doc << "<footer>" << str << "</footer>\n";
}

std::string HTMLDocBuilder::get() const { return doc.str(); }
