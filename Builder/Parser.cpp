#include "Parser.h"
#include "I_DocBuilder.h"

void Parser::construct() {
  if (builder) {
    builder->header("Title of the document");
    builder->body("The content of the document......");
    builder->footer("Posted by: Feabhas Ltd");
  }
}

void connect(Parser &parser, I_DocBuilder &builder) {
  parser.builder = &builder;
}
