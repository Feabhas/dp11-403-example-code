#include <iostream>
using namespace std;

#include "HTMLDocBuilder.h"
#include "Parser.h"
#include "PlainDocBuilder.h"

int main() {

  HTMLDocBuilder html_builder{};
  Parser parser{html_builder};

  parser.construct();
  cout << html_builder.get() << endl;

  PlainDocBuilder plain_builder{};
  connect(parser, plain_builder);

  parser.construct();
  cout << plain_builder.get() << endl;
}
