#ifndef HTMLDocBuilder_H
#define HTMLDocBuilder_H

#include "I_DocBuilder.h"
#include <sstream>
#include <string>

class HTMLDocBuilder : public I_DocBuilder {
public:
  void header(std::string_view str) override;
  void body(std::string_view str) override;
  void footer(std::string_view str) override;

  std::string get() const;

private:
  std::stringstream doc{};
};

#endif
