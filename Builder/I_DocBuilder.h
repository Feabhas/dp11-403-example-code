#ifndef I_DocBuilder_H
#define I_DocBuilder_H

#include <string_view>

class I_DocBuilder {
public:
  virtual void header(std::string_view str) = 0;
  virtual void body(std::string_view str) = 0;
  virtual void footer(std::string_view str) = 0;

  virtual ~I_DocBuilder() {}
};

#endif
