#ifndef PlainDocBuilder_H
#define PlainDocBuilder_H

#include "I_DocBuilder.h"
#include <sstream>
#include <string_view>

class PlainDocBuilder : public I_DocBuilder {
public:
  void header(std::string_view str) override;
  void body(std::string_view str) override;
  void footer(std::string_view str) override;

  std::string get() const;

private:
  std::stringstream doc{};
};

#endif // PlainDocBuilder_H
