#include "PlainDocBuilder.h"

void PlainDocBuilder::header(std::string_view str) { doc << str << "\n"; }

void PlainDocBuilder::body(std::string_view str) { doc << str << "\n"; }

void PlainDocBuilder::footer(std::string_view str) { doc << str << "\n"; }

std::string PlainDocBuilder::get() const { return doc.str(); }
