#ifndef Parser_H
#define Parser_H

class I_DocBuilder;

class Parser {
public:
  explicit Parser(I_DocBuilder &builder_) : builder{&builder_} {}
  void construct();

private:
  // Binding to Builder
  //
  friend void connect(Parser &parser, I_DocBuilder &builder);
  I_DocBuilder *builder{};
};

#endif
