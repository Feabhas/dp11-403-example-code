#include "State2.h"

#include <iostream>
using namespace std;

#include "State3.h"

State2::State2() { cout << "Entering State 2\n"; }
State2::~State2() { cout << "Exiting  State 2\n"; }

StateMachine::ptr State2::on_event1() { return StateMachine::no_transition(); }
StateMachine::ptr State2::on_event2() {
  //  work
  return StateMachine::transition_to<State3>();
}
StateMachine::ptr State2::on_event3() { return StateMachine::no_transition(); }
