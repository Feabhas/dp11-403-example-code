#ifndef State1_H
#define State1_H

#include "StateMachine.h"

class State1 : public StateMachine {
public:
  State1();
  ~State1();
  // protected:
  StateMachine::ptr on_event1() override;
  StateMachine::ptr on_event2() override;
  StateMachine::ptr on_event3() override;
};

#endif // State1_H