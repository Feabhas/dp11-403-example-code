#ifndef State2_H
#define State2_H

#include "StateMachine.h"

class State2 : public StateMachine {
public:
  State2();
  ~State2();
  StateMachine::ptr on_event1() override;
  StateMachine::ptr on_event2() override;
  StateMachine::ptr on_event3() override;
};

#endif // State2_H