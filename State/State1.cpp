#include "State1.h"

#include <iostream>
using namespace std;

#include "State2.h"

State1::State1() { cout << "Entering State 1\n"; }
State1::~State1() { cout << "Exiting  State 1\n"; }

StateMachine::ptr State1::on_event1() {
  return StateMachine::transition_to<State2>();
}
StateMachine::ptr State1::on_event2() { return StateMachine::no_transition(); }
StateMachine::ptr State1::on_event3() { return StateMachine::no_transition(); }
