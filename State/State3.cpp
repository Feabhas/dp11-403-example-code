#include "State3.h"

#include <iostream>
using namespace std;

#include "State2.h"
#include "State1.h"

State3::State3() { cout << "Entering State 3\n"; }
State3::~State3() { cout << "Exiting  State 3\n"; }

StateMachine::ptr State3::on_event1() { return StateMachine::no_transition(); }
StateMachine::ptr State3::on_event2() {
  return StateMachine::transition_to<State2>();
}
StateMachine::ptr State3::on_event3() {
  return StateMachine::transition_to<State1>();
}
