#ifndef State3_H
#define State3_H

#include "StateMachine.h"

class State3 : public StateMachine {
public:
  State3();
  ~State3();
  StateMachine::ptr on_event1() override;
  StateMachine::ptr on_event2() override;
  StateMachine::ptr on_event3() override;
};

#endif // State3_H