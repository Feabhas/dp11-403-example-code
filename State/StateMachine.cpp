#include "StateMachine.h"

#include "State1.h"

StateMachine::ptr StateMachine::initialise() {
  return std::make_unique<State1>();
}
