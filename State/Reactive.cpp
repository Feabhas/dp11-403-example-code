#include "Reactive.h"
#include "StateMachine.h"

void Reactive::event1() {
  if (auto new_state = current_state->on_event1()) {
    current_state = std::move(new_state);
  }
}

void Reactive::event2() {
  if (auto new_state = current_state->on_event2()) {
    current_state = std::move(new_state);
  }
}

void Reactive::event3() {
  if (auto new_state = current_state->on_event3()) {
    current_state = std::move(new_state);
  }
}
