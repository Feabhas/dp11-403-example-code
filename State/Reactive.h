#ifndef Reactive_H
#define Reactive_H

#include "StateMachine.h"

class Reactive {
public:
  Reactive() = default;
  // ~Reactive();

  void event1();
  void event2();
  void event3();

private:
  StateMachine::ptr current_state{StateMachine::initialise()};
};

#endif // Reactive_H