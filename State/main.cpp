#include "Reactive.h"

int main() {
  Reactive reactive{};

  reactive.event1(); // transition to State 2
  reactive.event1(); // no transition
  reactive.event2(); // transition to State 3
  reactive.event2(); // transition to State 2
}