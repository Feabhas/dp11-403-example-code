#ifndef StateMachine_H
#define StateMachine_H

#include <memory>

class StateMachine {
public:
  using ptr = std::unique_ptr<StateMachine>;

  virtual ~StateMachine() = default;

  static ptr initialise();

  virtual ptr on_event1() = 0;
  virtual ptr on_event2() = 0;
  virtual ptr on_event3() = 0;

protected:
  ptr no_transition() { return nullptr; }

  template <typename State_Ty> StateMachine::ptr transition_to();
};

template <typename State_Ty> StateMachine::ptr StateMachine::transition_to() {
  return std::make_unique<State_Ty>();
}

#endif // StateMachine_H