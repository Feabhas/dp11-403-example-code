#include "Client.h"
#include "Motor.h"

int main() {
  Motor motor{};
  Client client{motor.port};

  client.do_stuff();
}
