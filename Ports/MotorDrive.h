#ifndef MotorDrive_H
#define MotorDrive_H

#include <iostream>
using namespace std;

class MotorDrive {
public:
  void power_on() { cout << "motor powered on...\n"; }
  void power_off() { cout << "motor powered off...\n"; }
};

#endif