#include "Client.h"
#include "I_Motor.h"

void Client::do_stuff() {
  motor->start();
  motor->stop();
}

void connect(Client &client, I_Motor &m) { client.motor = &m; }
