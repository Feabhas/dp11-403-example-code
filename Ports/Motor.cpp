#include "Motor.h"

void Motor::Port::start() { drive->power_on(); }

void Motor::Port::stop() { drive->power_off(); }
