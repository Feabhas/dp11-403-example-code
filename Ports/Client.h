#ifndef Client_H
#define Client_H

class I_Motor;

class Client {
public:
  explicit Client(I_Motor &m) : motor{&m} {}
  void do_stuff();

private:
  I_Motor *motor;
  friend void connect(Client &client, I_Motor &m);
};

#endif // Client_H