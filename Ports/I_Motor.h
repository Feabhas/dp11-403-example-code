#ifndef I_Motor_H
#define I_Motor_H

class I_Motor {
public:
  virtual void start() = 0;
  virtual void stop() = 0;
  virtual ~I_Motor() = default;
};

#endif