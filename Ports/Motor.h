#ifndef Motor_H
#define Motor_H

#include "I_Motor.h"
#include "MotorDrive.h"

class Motor {
public:
  Motor() = default;

private:
  MotorDrive drive{};

  class Port : public I_Motor {
  public:
    Port(MotorDrive &md) : drive{&md} {}
    ~Port() override = default;

  private:
    void start() override;
    void stop() override;

    MotorDrive *drive;
  };

public:
  Port port{drive};
};

#endif // Motor_H