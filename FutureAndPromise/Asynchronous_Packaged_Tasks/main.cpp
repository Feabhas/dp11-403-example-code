#include <future>
#include <iostream>
#include <stdexcept>
#include <thread>
using namespace std;

#include <chrono> // namespace chrono
#include <future>
#include <thread>

using namespace std;
using namespace chrono;
using namespace chrono_literals;

double complex_calc( double x, double y )
{
  // Complex calculations here...
  volatile double result{ x };
  cout << "Starting calculation..." << endl;
  for ( int i{}; i < 1'000'000; ++i ) {
    result += x / y;
  }
  cout << "Calculation ready..." << endl;

  return result;
}

int main()
{
  future<double> result = async( complex_calc, 10.4, 17.6 );

  // Some time later...
  this_thread::sleep_for( 1s );

  cout << "Main asking for result..." << endl;

  double d = result.get();

  cout << "Result is : " << d << endl;
}
