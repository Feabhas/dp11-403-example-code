#include <future>
#include <iostream>
#include <thread>
using namespace std;

#include <future>
#include <thread>

using namespace std;

void get_input( promise<string>& p )
{
  cout << "Select option : " << endl;
  char   c = cin.get();
  string str{ "Input was: " };
  str += c;

  p.set_value( move( str ) );
}

int main()
{
  promise<string> p;
  thread          t1{ get_input, std::ref( p ) };

  t1.detach();

  this_thread::sleep_for( 1s );

  // Access the future from the promise...
  //
  auto result = p.get_future();

  cout << result.get() << endl;
}
