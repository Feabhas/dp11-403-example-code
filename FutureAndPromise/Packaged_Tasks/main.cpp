#include <future>
#include <iostream>
#include <stdexcept>
#include <thread>
using namespace std;

#include <chrono> // namespace chrono
#include <future>
#include <thread>

using namespace std;
using namespace chrono;
using namespace chrono_literals;

double complex_calc( double x, double y )
{
  // Complex calculations here...
  volatile double result{ x };
  cout << "Starting calculation..." << endl;
  for ( int i{}; i < 1'000'000; ++i ) {
    result += x / y;
  }
  cout << "Calculation ready..." << endl;

  return result;
}

int main()
{
  packaged_task<double( double, double )> task{ complex_calc };

  auto result = task.get_future();

  thread t{ move( task ), 10.4, 17.6 };
  t.detach();

  this_thread::sleep_for( 1s );

  // Some time later...
  cout << "Main asking for result..." << endl;

  double d = result.get();

  cout << "Result is : " << d << endl;
}
