#ifndef Component_A_H
#define Component_A_H

#include "Component.h"

class Component_A : public Component {
public:
  Component_A() = default;

  ~Component_A() override;
  void operation() override;
};

#endif
