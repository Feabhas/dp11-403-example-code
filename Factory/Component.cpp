#include "Component.h"
#include "Component_A.h"
#include "Component_B.h"

#include <iostream>

std::unique_ptr<Component> Component::make(std::string_view str) {
  using namespace std::literals;

  if (str == "App_A"sv)
    return std::make_unique<Component_A>();

  if (str == "App_B"sv)
    return std::make_unique<Component_B>();

  return std::unique_ptr<Component>();
}

void Component::operation() { std::cout << "Component::operation()\n"; }