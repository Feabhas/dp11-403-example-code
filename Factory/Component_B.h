#ifndef Component_B_H
#define Component_B_H

#include "Component.h"

class Component_B : public Component {
public:
  Component_B() = default;

  ~Component_B() override;
  void operation() override;
};

#endif
