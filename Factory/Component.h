#ifndef Component_H
#define Component_H

#include <memory>
#include <string_view>

class Component {
public:
  static std::unique_ptr<Component> make(std::string_view str);

  virtual void operation() = 0;
  virtual ~Component() = default;

protected:
  Component() = default;
};

#endif
