#include "Component.h"
#include <memory>

int main() {
  auto component1 = Component::make("App_A");
  component1->operation();

  auto component2 = Component::make("App_B");
  component2->operation();
}
