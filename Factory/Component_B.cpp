#include "Component_B.h"
#include <iostream>

void Component_B::operation() { std::cout << "Component_B::operation()\n"; }

Component_B::~Component_B() { std::cout << "~Component_B\n"; }