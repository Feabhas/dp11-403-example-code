#include "Component_A.h"
#include <iostream>

void Component_A::operation() { std::cout << "Component_A::operation()\n"; }

Component_A::~Component_A() { std::cout << "~Component_A\n"; }
