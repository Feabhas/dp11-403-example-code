#include "Spooler.h"

#include <iostream>
using namespace std;

std::unique_ptr<Spooler> Spooler::instance{nullptr};

Spooler &Spooler::get_instance() {
  if (instance == nullptr) {
    instance = std::unique_ptr<Spooler>{new Spooler{}};
  }
  return *instance;
}
