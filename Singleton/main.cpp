#include "Spooler.h"

#include <iostream>
using namespace std;

int main() {
#if 0
    Spooler spooler { };   // FAIL – Cannot create instance!
#endif

  auto &spooler_ptrA = Spooler::get_instance();
  auto &spooler_ptrB = Spooler::get_instance();

  cout << "Address of spooler_ptrA : " << hex << &spooler_ptrA << endl;
  cout << "Address of spooler_ptrB : " << hex << &spooler_ptrB << endl;
}
