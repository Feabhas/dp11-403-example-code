#ifndef Spooler_H
#define Spooler_H

#include <memory>

class Spooler {
public:
  static Spooler &get_instance();

  Spooler(const Spooler &) = delete;
  Spooler(const Spooler &&) = delete;
  Spooler &operator=(const Spooler &) = delete;
  Spooler &operator=(const Spooler &&) = delete;

protected:
  Spooler() = default;

private:
  static std::unique_ptr<Spooler> instance;
  int value{};
};

#endif // Spooler_H
