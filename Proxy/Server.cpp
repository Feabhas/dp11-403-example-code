#include "Server.h"
#include <iostream>
using namespace std;

void Server::read(Data &inout_data) {
  inout_data = the_data;
  cout << "Data retrieved from server...\n";
}

void Server::write(const Data &in_data) {
  the_data = in_data;
  cout << "Data updated on server...\n";
}
