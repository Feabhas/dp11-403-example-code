#include "Client.h"
#include "Proxy.h"

int main() {
  Proxy proxy{};
  Client client{proxy};

  client.do_stuff();
}
