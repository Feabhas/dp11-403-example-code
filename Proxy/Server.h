#ifndef Server_H
#define Server_H

#include "Data.h"
#include "I_Server.h"

class Server : public I_Server {
public:
  Server() = default;
  ~Server() override = default;

  void read(Data &inout_data) override;
  void write(const Data &in_data) override;

private:
  Data the_data{42};
};

#endif