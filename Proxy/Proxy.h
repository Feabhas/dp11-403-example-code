#ifndef Proxy_H
#define Proxy_H

#include "Data.h"
#include "I_Server.h"
#include <memory>

#if __GNUC__
#include "Server.h"
#else
class Server;
#endif

class Proxy : public I_Server {
public:
  Proxy() = default;

  void read(Data &inout_data) override;
  void write(const Data &in_data) override;

private:
  Data cache{};
  bool invalid{true};

  std::unique_ptr<Server> server{};
  void lazy_init();
};

#endif