#ifndef I_Server_H
#define I_Server_H

struct Data;

class I_Server {
public:
  virtual void read(Data &inout_data) = 0;
  virtual void write(const Data &in_data) = 0;
  virtual ~I_Server() = default;
};

#endif
