#ifndef Client_H
#define Client_H

class I_Server;

class Client {
public:
  explicit Client(I_Server &serv) : server{&serv} {}
  void do_stuff();

private:
  I_Server *server{};
  friend void connect(Client &client, I_Server &serv);
};

#endif
