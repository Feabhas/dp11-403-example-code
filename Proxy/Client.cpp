#include "Client.h"
#include "Data.h"
#include "I_Server.h"

#include <iostream>
using namespace std;

void Client::do_stuff() {
  Data data{};
  server->read(data);
  server->read(data);

  // Modify data...

  server->write(data);
  server->read(data);
}

void connect(Client &client, I_Server &serv) { client.server = &serv; }
