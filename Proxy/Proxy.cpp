#include "Proxy.h"
#include "Server.h"

void Proxy::lazy_init() {
  if (server == nullptr) {
    server = std::make_unique<Server>();
  }
}

void Proxy::read(Data &inout_data) {
  lazy_init();

  if (invalid) {
    server->read(cache);
    invalid = false;
  }
  inout_data = cache;
}

void Proxy::write(const Data &in_data) {
  lazy_init();

  server->write(in_data);
  invalid = true;
}
