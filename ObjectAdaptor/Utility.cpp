#include "Utility.h"
#include <iostream>
using namespace std;

void Utility::func1() {
  cout << "Utility::func1" << endl;
  utility_data += 1;
}

void Utility::func2() {
  cout << "Utility::func2" << endl;
  utility_data += 2;
}

void Utility::func3() {
  cout << "Utility::func3" << endl;
  utility_data += 3;
}

void Utility::func4() {
  cout << "Utility::func4" << endl;
  utility_data += 4;
}

void Utility::helper_function() {
  cout << "Utility value is " << utility_data << endl;
}
