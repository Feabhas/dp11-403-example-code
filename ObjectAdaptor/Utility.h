#ifndef Utility_H
#define Utility_H

class Utility {
public:
  Utility() = default;
  void func1();
  void func2();
  void func3();
  void func4();

protected:
  void helper_function();

private:
  int utility_data{};
};

#endif
