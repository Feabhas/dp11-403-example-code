#ifndef ObjectAdapter_H
#define ObjectAdapter_H

#include "I_Service.h"
#include "Utility.h"

class ObjectAdapter : public I_Service {
public:
  ObjectAdapter() = default;

private:
  void service1() override;
  void service2() override;
  void service3() override;
  void service4() override;

  Utility utility{};
};

#endif
