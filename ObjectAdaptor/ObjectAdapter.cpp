#include "ObjectAdapter.h"

void ObjectAdapter::service1() // Simple pass-through call...
{
  utility.func1();
}

void ObjectAdapter::service2() // Combining behaviours...
{
  utility.func1();
  utility.func2();
}

void ObjectAdapter::service3() { utility.func3(); }

void ObjectAdapter::service4() {
  //  utility.helper_function();     // ERROR - protected member
}
