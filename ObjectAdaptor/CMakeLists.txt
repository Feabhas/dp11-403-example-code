cmake_minimum_required(VERSION 2.6)

set(EXE_NAME    solution)
set(SOURCE_DIR  src)
set(OUTPUT_DIR  bin)

project(${EXE_NAME} LANGUAGES CXX)
set(CMAKE_CXX_STANDARD 17)
set(_REQUIRED ONCMAKE_CXX_STANDARD)

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
set(CMAKE_CXX_CLANG_TIDY "clang-tidy;-checks=-*,cppcoreguidelines-*,clang-analyzer-*,modernize-*,-modernize-use-trailing-return-type")

add_compile_options(-Wall -Wextra -g -Og)
link_libraries(-pthread)
include_directories(${SOURCE_DIR})


set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${OUTPUT_DIR})

add_executable(${EXE_NAME} 
    Client.cpp
    main.cpp
    ObjectAdapter.cpp
    Utility.cpp
)

