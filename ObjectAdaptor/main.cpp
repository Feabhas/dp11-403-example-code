#include "Client.h"
#include "ObjectAdapter.h"

int main() {
  ObjectAdapter adapter{};
  Client client{adapter};

  // connect(client, adapter);
  client.do_stuff();
}
