#ifndef Client_H
#define Client_H

class I_Service;

class Client {
public:
  explicit Client(I_Service &serv) : server{&serv} {}
  void do_stuff();

private:
  I_Service *server{};
  friend void connect(Client &client, I_Service &serv);
};

#endif
