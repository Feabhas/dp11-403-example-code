#include "ModBus.h"

#include <iostream>
using namespace std;

void ModBus::physical_send() { cout << "ModBus::physical_send\n"; }
void ModBus::transport_connect() { cout << "ModBus::transport_connect\n"; }
void ModBus::network_message() { cout << "ModBus::network_message\n"; }
