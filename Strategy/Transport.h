#ifndef Transport_H
#define Transport_H

class I_Strategy;

class Transport {
public:
  void set_strategy(I_Strategy &strat);
  void connect();

private:
  I_Strategy *strategy{};
};

#endif // Transport_H