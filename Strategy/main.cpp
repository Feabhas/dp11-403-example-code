#include "ModBus.h"
#include "Network.h"
#include "Physical.h"
#include "TCP.h"
#include "Transport.h"

int main() {
  TCP tcp{};
  ModBus Mbus{};

  Physical physical{};
  Transport transport{};
  Network network{};

  // Set initial strategy...
  //
  physical.set_strategy(tcp);
  transport.set_strategy(tcp);
  network.set_strategy(tcp);

  // Use networking components...
  physical.send();
  transport.connect();
  network.send_message();

  // Change strategy...
  //
  physical.set_strategy(Mbus);
  transport.set_strategy(Mbus);
  network.set_strategy(Mbus);

  ///...
  // Use networking components...
  physical.send();
  transport.connect();
  network.send_message();
}
