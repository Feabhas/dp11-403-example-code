#ifndef TCP_H
#define TCP_H

#include "I_Strategy.h"

class TCP : public I_Strategy {
private:
  void physical_send() override;
  void transport_connect() override;
  void network_message() override;
};

#endif // TCP_H