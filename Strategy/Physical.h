#ifndef Physical_H
#define Physical_H

class I_Strategy;

class Physical {
public:
  void set_strategy(I_Strategy &strat);
  void send();

private:
  I_Strategy *strategy{};
};

#endif // Physical_H