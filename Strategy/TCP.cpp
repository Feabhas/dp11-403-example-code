#include "TCP.h"

#include <iostream>
using namespace std;

void TCP::physical_send() { cout << "TCP::physical_send\n"; }
void TCP::transport_connect() { cout << "TCP::transport_connect\n"; }
void TCP::network_message() { cout << "TCP::network_message\n"; }
