#include "Transport.h"

#include "I_Strategy.h"

void Transport::set_strategy(I_Strategy &strat) { strategy = &strat; }
void Transport::connect() {
  if (strategy)
    strategy->transport_connect();
}