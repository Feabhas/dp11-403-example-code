#ifndef Network_H
#define Network_H

class I_Strategy;

class Network {
public:
  void set_strategy(I_Strategy &strat);
  void send_message();

private:
  I_Strategy *strategy{};
};

#endif // Network_H