#include "Physical.h"

#include "I_Strategy.h"

void Physical::set_strategy(I_Strategy &strat) { strategy = &strat; }
void Physical::send() {
  if (strategy)
    strategy->physical_send();
}
