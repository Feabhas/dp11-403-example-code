#ifndef I_Strategy_H
#define I_Strategy_H

class I_Strategy {
public:
  virtual void physical_send() = 0;
  virtual void transport_connect() = 0;
  virtual void network_message() = 0;

  virtual ~I_Strategy() = default;
};

#endif // I_Strategy_H