#include "Network.h"

#include "I_Strategy.h"

void Network::send_message() {
  // Code...

  if (strategy)
    strategy->network_message();

  // More code...
}

void Network::set_strategy(I_Strategy &strat) { strategy = &strat; }
