#ifndef ModBus_H
#define ModBus_H

#include "I_Strategy.h"

class ModBus : public I_Strategy {
private:
  void physical_send() override;
  void transport_connect() override;
  void network_message() override;
};

#endif // ModBus_H