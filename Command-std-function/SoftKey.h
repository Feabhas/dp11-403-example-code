#ifndef SoftKey_H
#define SoftKey_H

class I_Command;

#include <functional>

class SoftKey {
public:
  void on_select();

private:
  friend void connect(SoftKey &key, std::function<void(void)> f);
  std::function<void(void)> command{};
};

#endif // SoftKey_H