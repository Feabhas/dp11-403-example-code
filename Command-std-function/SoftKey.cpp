#include "SoftKey.h"
#include "I_Command.h"

void SoftKey::on_select() {
  if (command) {
    command();
  }
}

// void connect(SoftKey& key, I_Command& cmd)
// {
//   key.command = &cmd;
// }

void connect(SoftKey &key, std::function<void(void)> f) { key.command = f; }
