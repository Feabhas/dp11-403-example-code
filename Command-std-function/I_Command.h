#ifndef I_Command_H
#define I_Command_H

class I_Command {
public:
  virtual void operator()() = 0;
  virtual ~I_Command() = default;
};

#endif // I_Command_H