#include "MFD.h"
#include "I_Command.h"

void MFD::set_behaviour(KeyID key, std::function<void(void)> cmd) {
  connect(keys[static_cast<size_t>(key)], cmd);
}

void MFD::SK_pressed(KeyID key) { keys[static_cast<size_t>(key)].on_select(); }
