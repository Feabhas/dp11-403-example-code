#ifndef Commands_H
#define Commands_H

#include "I_Command.h"

class Radar;

class ClearTarget : public I_Command {
public:
  void operator()() override;

private:
  Radar *radar{};
};

class WeaponRack;

class JettisonStore : public I_Command {
public:
  void operator()() override;

private:
  WeaponRack *weapon_rack{};
};

class IncreaseChaff : public I_Command {
public:
  void operator()() override;
};

class DecreaseChaff : public I_Command {
public:
  void operator()() override;
};

class NullCmd : public I_Command {
public:
  void operator()() override {}
};

#endif // Commands_H