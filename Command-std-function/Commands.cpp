#include "Commands.h"

#include <iostream>
using namespace std;

void ClearTarget::operator()() {
  cout << "ClearTarget" << endl;
  // radar->mf()
}

void JettisonStore::operator()() {
  cout << "JettisonStore" << endl;
  // weapon_rack->mf()
}

void DecreaseChaff::operator()() { cout << "DecreaseChaff" << endl; }

void IncreaseChaff::operator()() { cout << "IncreaseChaff" << endl; }
