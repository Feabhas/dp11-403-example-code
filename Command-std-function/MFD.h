#ifndef MFD_H
#define MFD_H

#include "SoftKey.h"
#include <array>

#include <functional>

class MFD {
public:
  enum class KeyID { sk1, sk2, sk3, sk4 };
  void set_behaviour(KeyID key, std::function<void(void)> cmd);

  void SK_pressed(KeyID key);

private:
  constexpr static std::size_t num_keys{4};
  std::array<SoftKey, num_keys> keys{};
};

#endif // MFD_H