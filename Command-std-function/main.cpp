#include "Commands.h"
#include "MFD.h"

#include <iostream>
using namespace std;

void f() { cout << "local function\n"; }

int main() {
  MFD display{};

  JettisonStore jettison{};
  IncreaseChaff incr_chaff{};
  DecreaseChaff decr_chaff{};
  ClearTarget clear_tgt{};

  // NullCmd null{};
  auto null = [] {};

  display.set_behaviour(MFD::KeyID::sk1, jettison);
  display.set_behaviour(MFD::KeyID::sk2, null);
  display.set_behaviour(MFD::KeyID::sk3, null);
  display.set_behaviour(MFD::KeyID::sk4, null);

  display.SK_pressed(MFD::KeyID::sk1);
  display.SK_pressed(MFD::KeyID::sk2);
  display.SK_pressed(MFD::KeyID::sk3);
  display.SK_pressed(MFD::KeyID::sk4);

  cout << "+++ Changing Mode +++" << endl;
  display.set_behaviour(MFD::KeyID::sk1, incr_chaff);
  display.set_behaviour(MFD::KeyID::sk2, decr_chaff);
  display.set_behaviour(MFD::KeyID::sk3, f);
  display.set_behaviour(MFD::KeyID::sk4, [] { cout << "lambda\n"; });

  display.SK_pressed(MFD::KeyID::sk1);
  display.SK_pressed(MFD::KeyID::sk2);
  display.SK_pressed(MFD::KeyID::sk3);
  display.SK_pressed(MFD::KeyID::sk4);
}
