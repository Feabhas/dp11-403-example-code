#include "ClassAdapter.h"

void ClassAdapter::service1() // Simple pass-through call...
{
  Utility::func1();
}

void ClassAdapter::service2() // Combining behaviours...
{
  Utility::func1();
  Utility::func2();
}

void ClassAdapter::service3() { Utility::func3(); }

void ClassAdapter::service4() {
  Utility::helper_function(); // no longer error - protected member
}
