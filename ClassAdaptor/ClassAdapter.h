#ifndef ClassAdapter_H
#define ClassAdapter_H

#include "I_Service.h"
#include "Utility.h"

class ClassAdapter : public I_Service, // Interface
                     private Utility   // Implementation
{
public:
  ClassAdapter() = default;

private:
  void service1() override;
  void service2() override;
  void service3() override;
  void service4() override;
};

#endif
