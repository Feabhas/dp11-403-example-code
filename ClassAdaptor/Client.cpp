#include "Client.h"
#include "I_Service.h"

void Client::do_stuff() {
  server->service1();
  server->service2();
  server->service3();
  server->service4();
}

void connect(Client &client, I_Service &serv) { client.server = &serv; }
