#ifndef I_Service_H
#define I_Service_H

class I_Service {
public:
  virtual void service1() = 0;
  virtual void service2() = 0;
  virtual void service3() = 0;
  virtual void service4() = 0;
};

#endif
