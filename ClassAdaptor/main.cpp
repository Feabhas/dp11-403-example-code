#include "ClassAdapter.h"
#include "Client.h"

int main() {
  ClassAdapter adapter{};
  Client client{adapter};

  client.do_stuff();
}
