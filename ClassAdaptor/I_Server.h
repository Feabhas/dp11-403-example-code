#ifndef I_Server_H
#define I_Server_H

class I_Server {
public:
  virtual void read(Data &inout_data) = 0;
  virtual void write(const Data &in_data) = 0;
};

#endif
