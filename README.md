# Simple Example Code for C++11 Design Pattern Course

Examples include:

* Singleton :-(
* Meyers Singleton :-{
* Factory
* Factory Method
* Builder
* Object Adaptor
* Class Adaptor
* Proxy
* Ports
* Decorator
* Bridge
* Observer
* State
* Strategy
* Threading
* Mutual Exclusion
* Guarded Suspension
* Asynchronous_Message
* Futures