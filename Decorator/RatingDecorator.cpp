#include "RatingDecorator.h"
#include <array>
#include <iostream>
#include <string>
#include <unordered_map>

using namespace std;

static unordered_map<RatingDecorator::Watts, std::string> wattage = {
    {RatingDecorator::Watts::UNDEFINED, "UNDEFINED"},
    {RatingDecorator::Watts::w40, "40w"},
    {RatingDecorator::Watts::w60, "60w"},
    {RatingDecorator::Watts::w75, "75w"},
    {RatingDecorator::Watts::w100, "100w"},
    {RatingDecorator::Watts::w150, "150w"}};

void RatingDecorator::on() {
  cout << wattage[rating] << "  : ";
  Decorator::on();
}

void RatingDecorator::off() {
  cout << wattage[rating] << "  : ";
  Decorator::off();
}
