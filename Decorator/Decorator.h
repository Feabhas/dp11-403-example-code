#ifndef Decorator_H
#define Decorator_H

#include "I_Device.h"

class Decorator : public I_Device {
protected:
  void on() override = 0;
  void off() override = 0;

private:
  I_Device *decorated_object{nullptr};
  friend void decorate(I_Device &dev, Decorator &dec);
};

#endif // Decorator_H