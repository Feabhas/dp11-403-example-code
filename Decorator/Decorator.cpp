#include "Decorator.h"

void decorate(I_Device &dev, Decorator &dec) { dec.decorated_object = &dev; }

void Decorator::on() {
  if (decorated_object) {
    decorated_object->on();
  }
}

void Decorator::off() {
  if (decorated_object) {
    decorated_object->off();
  }
}
