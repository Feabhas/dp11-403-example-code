#include "NameDecorator.h"
#include <iostream>
using namespace std;

void NameDecorator::on() {
  cout << name << " : ";
  Decorator::on();
}

void NameDecorator::off() {
  cout << name << " : ";
  Decorator::off();
}
