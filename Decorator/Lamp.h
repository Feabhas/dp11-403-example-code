#ifndef Lamp_H
#define Lamp_H

#include "I_Device.h"

class Lamp : public I_Device {
public:
  enum class Location { unknown, room_A, room_B };
  enum class Device { invalid, dev_1, dev_2, dev_3, dev_4 };

  Lamp(Location loc, Device dev) : location{loc}, device{dev} {}
  ~Lamp() override = default;

protected:
  void on() override;
  void off() override;

private:
  void display_name();
  Location location{};
  Device device{};
};

#endif // Lamp_H