#include "Lamp.h"

#include <array>
#include <iostream>
#include <string_view>
using namespace std;

constexpr size_t num_of_locations = 3;
constexpr size_t num_of_devices = 5;

constexpr array<string_view, num_of_locations> Location_names{
    "UNKNOWN", "ROOM A", "ROOM B"};
constexpr array<string_view, num_of_devices> Device_names{
    "INVALID", "DEV 1", "DEV 2", "DEV 3", "DEV 4"};

void Lamp::display_name() {
  cout << "Lamp in " << Location_names.at(static_cast<std::size_t>(location))
       << ":" << Device_names.at(static_cast<std::size_t>(device));
}
void Lamp::on() {
  display_name();
  cout << " is on...\n";
}
void Lamp::off() {
  display_name();
  cout << " is off...\n";
}