#ifndef Client_H
#define Client_H

class I_Device;

class Client {
public:
  Client() = default;
  explicit Client(I_Device &dev) : device(&dev) {}
  void do_stuff();

private:
  I_Device *device{};
  friend void connect(Client &c, I_Device &dev);
};

#endif // Client_H