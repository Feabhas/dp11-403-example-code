#ifndef RatingDecorator_H
#define RatingDecorator_H

#include "Decorator.h"
#include <string>

class RatingDecorator : public Decorator {
public:
  enum class Watts { UNDEFINED, w40, w60, w75, w100, w150 };
  RatingDecorator(Watts w) : rating{w} {}

protected:
  void on() override;
  void off() override;

private:
  Watts rating{Watts::UNDEFINED};
};

#endif // NameDecorator_H