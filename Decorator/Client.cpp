#include "Client.h"
#include "I_Device.h"

void Client::do_stuff() {
  device->on();
  device->off();
}

void connect(Client &c, I_Device &dev) { c.device = &dev; }
