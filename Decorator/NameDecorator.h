#ifndef NameDecorator_H
#define NameDecorator_H

#include "Decorator.h"
#include <string_view>

class NameDecorator : public Decorator {
public:
  explicit NameDecorator(std::string_view str) : name{str} {}

protected:
  void on() override;
  void off() override;

private:
  std::string_view name;
};

#endif // NameDecorator_H