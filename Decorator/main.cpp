#include "Client.h"
#include "Lamp.h"
#include "NameDecorator.h"
#include "RatingDecorator.h"

#include <iostream>

int main() {
  Lamp lamp{Lamp::Location::room_A, Lamp::Device::dev_1};
  Client client{lamp};
  client.do_stuff();

  ////////////////////////////////////////////////////////

  NameDecorator name{"Desk Lamp"};
  decorate(lamp, name);
  connect(client, name);
  // client.do_stuff();

  ////////////////////////////////////////////////////////

  // RatingDecorator rating{ RatingDecorator::Watts::w60 };
  // decorate(lamp, rating);
  // connect(client, rating);
  // client.do_stuff();

  ////////////////////////////////////////////////////////

  // decorate(name, rating);
  // connect(client, rating);

  ////////////////////////////////////////////////////////

  client.do_stuff();
}
