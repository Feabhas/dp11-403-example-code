#ifndef I_Device_H
#define I_Device_H

class I_Device {
public:
  virtual void on() = 0;
  virtual void off() = 0;
  virtual ~I_Device() = default;
};

#endif // I_Device_H