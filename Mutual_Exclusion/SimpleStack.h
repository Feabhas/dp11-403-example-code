#ifndef SimpleStack_H
#define SimpleStack_H

#include <cstdint>
#include <mutex>

class SimpleStack {
public:
  bool push(int val);
  int pop();

private:
  static constexpr uint32_t sz{10};
  uint32_t count{0};
  int stack[sz];
  std::mutex mtx;
};

#endif // SimpleStack_H