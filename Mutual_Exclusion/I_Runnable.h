#ifndef I_Runnable_H
#define I_Runnable_H

class I_Runnable {
public:
  virtual void run() = 0;
  virtual ~I_Runnable() = default;
};

#endif // I_Runnable_H