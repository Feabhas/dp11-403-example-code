#include "Consumer.h"

#include "SimpleStack.h"

#include <chrono> // namespace chrono
#include <iostream>
#include <thread> // namespace this_thread

using namespace std;
using namespace chrono;
using namespace chrono_literals;

void Consumer::run() {
  for (int i = 0; i < 120; ++i) {
    this_thread::sleep_for(100ms);
    cout << '.';
    cout.flush();
    try {
      sum += stack->pop();
    } catch (...) {
    }
  }
  this_thread::sleep_for(500ms);

  cout << "Consumed: " << sum << endl;
}
