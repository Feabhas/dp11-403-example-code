#include "SimpleStack.h"

bool SimpleStack::push(int val) {
  std::lock_guard<std::mutex> guard{mtx};
  if (count < sz) {
    stack[count++] = val;
    return true;
  }
  return false;
} // UNLOCK

int SimpleStack::pop() {
  std::lock_guard<std::mutex> guard{mtx};

  if (count == 0) {
    throw std::range_error("Stack is empty!"); // UNLOCK
  }

  return stack[--count];
  ;
} // UNLOCK
