#ifndef Consumer_H
#define Consumer_H

#include "I_Runnable.h"

class SimpleStack;

class Consumer : public I_Runnable {
public:
  Consumer(SimpleStack &s) : stack{&s} {}

  void run();

private:
  int sum{};
  SimpleStack *stack{};
};

#endif // Consumer_H