#include <iostream>
#include <thread>
using namespace std;

#include "Consumer.h"
#include "Producer.h"
#include "SimpleStack.h"

int main() {
  SimpleStack ss{};
  Producer p{ss};
  Consumer c{ss};

  auto run_policy = [](I_Runnable &runnable) { runnable.run(); };

  thread t1{run_policy, std::ref(p)};
  thread t2{run_policy, std::ref(c)};

  t1.join();
  t2.join();
  cout << endl;
}
