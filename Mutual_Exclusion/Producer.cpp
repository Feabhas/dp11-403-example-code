#include "Producer.h"

#include "SimpleStack.h"

#include <chrono> // namespace chrono
#include <iostream>
#include <thread> // namespace this_thread

using namespace std;
using namespace chrono;
using namespace chrono_literals;

void Producer::run() {
  for (int i = 0; i < 100; ++i) {
    this_thread::sleep_for(100ms);
    cout << '+';
    cout.flush();
    stack->push(i);
    sum += i;
  }

  cout << "Produced: " << sum << endl;
}
