#include "Meyers_Singleton.h"

Meyers_Singleton &Meyers_Singleton::get_instance() {
  // Create a static automatic object.
  // Its memory will be allocated in the Statics
  // area at compile-time.
  // Its constructor will be called the first time
  // get_instance() is called.
  //
  static Meyers_Singleton instance{};
  return instance;
}
