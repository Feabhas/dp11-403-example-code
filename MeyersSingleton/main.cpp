#include "Meyers_Singleton.h"

#include <iostream>
#include <memory>
using namespace std;

int main() {
#if 0
    Meyers_Singleton spooler { };   // FAIL – Cannot create instance!
#endif

  auto &spooler_ptrA = Meyers_Singleton::get_instance();
  auto &spooler_ptrB = Meyers_Singleton::get_instance();

  cout << "Address of spooler_ptrA : " << hex << &spooler_ptrA << endl;
  cout << "Address of spooler_ptrB : " << hex << &spooler_ptrB << endl;
}
