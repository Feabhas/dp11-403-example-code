#ifndef Meyers_Singleton_H
#define Meyers_Singleton_H

class Meyers_Singleton {
public:
  static Meyers_Singleton &get_instance();

  Meyers_Singleton(const Meyers_Singleton &) = delete;
  Meyers_Singleton(const Meyers_Singleton &&) = delete;
  Meyers_Singleton &operator=(const Meyers_Singleton &) = delete;
  Meyers_Singleton &operator=(const Meyers_Singleton &&) = delete;

protected:
  Meyers_Singleton() = default;

private:
  int data{};
};

#endif // Meyers_Singleton_H
